# Exercise 3

1. Create a branch `exercise_3` from `master` branch.
2. Try to create GitLab release in `Deploy -> Releases` (see section 3.6.1 for more). You can do it manually or 
using GitLab's release-cli tool.
3. Let’s edit the pipeline, so it’s going to have:
   1. a `variables` section where you’re going to define global variables like pip cache dir, package version etc.
   2. a `cache` section to store a pip cache.
   3. a `artifacts` subsection to a `unit-test-job` section and force reports to be created always, in a format of 
   junit `report.xml` file with expiration time 1 week.
   4. a `timeout` subsection to each section with various values depending on a job type.
   5. a `release` stage and a new job added in which we create a new release.
      1. assign a unique `tag` on a branch from which binary package is created.
      2. optionally: add link(s) to the binaries to the release notes.
4. Test the pipeline you’ve created, check that all jobs have run and succeeded. Make sure you can see results, reports,
statistics, and you understand how to customize them according to your needs.
5. Merge your branch into `master` branch.

### Useful links:

- [variables](https://docs.gitlab.com/ee/ci/variables/)
- [common-uses-cases-for-caches](https://docs.gitlab.com/ee/ci/caching/#common-use-cases-for-caches)
- [unit-test-report-examples](https://docs.gitlab.com/ee/ci/testing/unit_test_report_examples.html#python-example)
- [release-cli-1](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs)
- [release-cli-2](https://docs.gitlab.com/ee/ci/yaml/#release)


                   			           GREAT JOB! YOU’VE MADE IT AGAIN!
                                                       CONGRATULATIONS!